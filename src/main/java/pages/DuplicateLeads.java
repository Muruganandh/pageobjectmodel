package pages;

import java.util.concurrent.TimeUnit;

import libraries.Annotations;

public class DuplicateLeads extends Annotations {
	
	public DuplicateLeads clickCreateLead() {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElementByXPath("//input[@id='createLeadForm_companyName']").clear();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElementById("createLeadForm_companyName").sendKeys("TNQ");
		return this;
		
	}
		
	public ViewLeadPage clickCreateData() {
		driver.findElementByXPath("//input[@value='Create Lead']").click();
		return new ViewLeadPage();
	}

	
}
