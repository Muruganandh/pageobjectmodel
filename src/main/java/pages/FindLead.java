package pages;

//import java.util.concurrent.TimeUnit;

//import org.openqa.selenium.chrome.ChromeDriver;
//import org.testng.annotations.Test;

import libraries.Annotations;

public class FindLead extends Annotations{
	
	
	
	public FindLead findLead() {
		driver.findElementById("x-form-el-ext-gen248").click();
		driver.findElementById("x-form-el-ext-gen248").sendKeys("vijay");
		return this;
	}
	
	public FindLead clickFindLead() {
		driver.findElementByLinkText("Find Leads").click();
		return this;
	}

	public FindLead copytext() {
		driver.findElementByXPath("//input[@name='id']").getText();
		return this;
		
	}
	
	
	public ViewLeadPage clickfinddata() {
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").click();
		return new ViewLeadPage();
	}
	
}






