package pages;

import libraries.Annotations;

public class MyLeadsPage extends Annotations {
	
	public CreateLeadPage clickCreateLead() {
		driver.findElementByLinkText("Create Lead")
		.click();
		return new CreateLeadPage();
		
	}
	
	public MyEditdata clickMyLead() {
		driver.findElementByLinkText("My Leads")
		.click();
		return new MyEditdata();
		
	}
	
	public FindLead clickfindLeads() {
		driver.findElementByLinkText("Find Leads").click();
		return new FindLead();
	}
	
	
	public ViewLeadPage clickduplicatedata() {
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").click();
		return new ViewLeadPage();
	}

}
