package pages;

import libraries.Annotations;

public class MyHomePage extends Annotations {
	
	public MyLeadsPage clickLeadsTab() {
		driver.findElementByLinkText("Leads").click();
		return new MyLeadsPage();
	}
	
	
	public FindLead clickFindLeadTab() {
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		return new FindLead();
	}
	
	

}
