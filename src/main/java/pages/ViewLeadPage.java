package pages;

import libraries.Annotations;

public class ViewLeadPage extends Annotations {
	
	public ViewLeadPage verifyFirstName() {
		String text = driver.findElementById("viewLead_firstName_sp")
		.getText();
		if(text.equals("vijay")) {
			System.out.println("First name matches with input data");
		}else {
			System.err.println("First name not matches with input data");
		}
		return this;
	}
	
	
	public MyLeadsPage deletefinddata() {
		driver.findElementByLinkText("Delete").click();
		return new MyLeadsPage();
	}
	
	public ViewLeadPage editfinddata() {
		driver.findElementByLinkText("Edit").click();
		return this;
	}
	
	public ViewLeadPage editmydata() {
		driver.findElementById("updateLeadForm_firstName").clear();
		return this;
	}
	
	public ViewLeadPage editdata() {
		driver.findElementById("updateLeadForm_firstName").sendKeys("anandh");
		return this;
	}
	
	public ViewLeadPage clickUpdate() {
		driver.findElementByXPath("//input[@value='Update']").click();
		return this;
	}
	
	public DuplicateLeads clickDupLead() {
		driver.findElementByLinkText("Duplicate Lead").click();
		return new DuplicateLeads();
		
	}



	
	
	
	
	
	
}
